
FROM node:8.11.1

RUN mkdir -p /app
WORKDIR /app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV
COPY package.json /app/
RUN npm install 

COPY . /app

EXPOSE 3000

CMD [ "npm", "start" ]