var express = require('express');
var router = express.Router();

var addfile = require('./../controllers/add');


/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Calculator', data:'' });
});


/* POST Add */
router.post('/add', addfile.addFun);

module.exports = router;
